# MALINS

[[_TOC_]]

## Technologie

### Casque Audio
- Sennheiser HD25 [:notebook:](https://fr-fr.sennheiser.com/casque-dj-on-ear-hd25) [:money_with_wings:](https://www.thomann.de/fr/sennheiser_hd_25.htm) :fire: *Léger et puissant. Pièces de rechange disponibles à la vente.*  
  ![Sennheiser HD25](/illustrations/HD25.jpg)  

- Beyerdynamic DT-770 [:notebook:](https://europe.beyerdynamic.com/amfile/file/download/file_id/3878/product_id/2912/) [:money_with_wings:](https://www.thomann.de/fr/beyerdynamic_dt770_pro80_ohm.htm) :fire: *Confortable et précis. Pièces de rechange disponibles à la vente.*  
  ![Beyerdynamic DT-770](/illustrations/DT770.jpg)  

### Montres
- Orient Bambino [:notebook:](https://www.lecalibre.com/orient-bambino/) [:money_with_wings:](https://ocarat.com/recherche?id_search=1&search=bambino&#marque-orient/-bambino/orderby-position/orderway-asc/n-42) :fire: *Calibre automatique japonais, rapport qualité-prix incroyable.*  
  ![orient bambino](/illustrations/Orient_Bambino.png)  
- Casio G-Shock GW-5610 [:notebook:](https://www.g-shock.eu/fr/montres/the-origin/gw-m5610u-1er/) [:money_with_wings:](https://ocarat.com/montre-g-shock-gw-m5610u-1er-casio-g-shock-77723.html) :fire: *Monstre solaire radiocontrolée. C’est le CHOC !*  

### Appareils photo compacts

Voir [le guide](guides/photo.md) pour choisir quel appareil convient le mieux.
Attention, pour se mettre sérieusement à la photo, ça vaut peut-être le coup de regarder du côté des boîtiers à objectifs interchangeables !

- Sony RX100 VII (ou VA / VI / V)
[:notebook:](https://www.dpreview.com/products/sony/compacts/sony_dscrx100m7)
[:money_with_wings:](https://www.digit-photo.com/SONY-Cyber-Shot-DSC-RX100-VII-rSONYDSCRX100M7.html)
- Canon G5 X II (ou G7 X III / G5 X)
[:notebook:](https://www.dpreview.com/products/canon/compacts/canon_g5xii)
[:money_with_wings:](https://www.digit-photo.com/CANON-Powershot-G5-X-Mark-II-Noir-rCANON3070C002.html)
- Panasonic LX15
[:notebook:](https://www.dpreview.com/products/panasonic/compacts/panasonic_dmclx10)
[:money_with_wings:](https://www.digit-photo.com/PANASONIC-Lumix-DMC-LX15-Noir-rPANADMCLX15EGK.html)
- Fujifilm X100V (ou X100F)
[:notebook:](https://www.dpreview.com/products/fujifilm/compacts/fujifilm_x100v)
[:money_with_wings:](https://www.digit-photo.com/FUJI-X100V-Silver-rFUJI16642939.html)
:fire:
- Ricoh GR III (ou IIIx / II)
[:notebook:](https://www.dpreview.com/products/ricoh/compacts/ricoh_griii)
[:money_with_wings:](https://www.digit-photo.com/RICOH-GR-III-Noir-rRICOH15038.html)

Source : www.dpreview.com

## Aventure

### Poches à eau

- Hydrapak Shape-Shift 3 L [:notebook:](https://www.hydrapak.fr/fr/poches-hydratation/951-857-shape-shift-3-l.html#/139-couleur_hydra_komp-clear) [:money_with_wings:](https://www.auvieuxcampeur.fr/reservoir-shape-shift.html0) :fire: *Très solide et simple à nettoyer ! Tube et valve et tétine de rechanche disponibles à la vente.*  
  ![Hydrapak Shap-Shift](/illustrations/hydrapak.jpg)  

### Lampes

- Nitecore HC-65 V2 [:notebook:](https://www.nitecore.fr/lampe-frontale-hc65-v2-1750lm-lg-908mm-nl1835hp-inclu-usb-c-c2x36444783) [:money_with_wings:](https://fr.aliexpress.com/wholesale?catId=0&initiative_id=AS_20220529054147&SearchText=nitecore+hc65+v2&spm=a2g0o.home.1000002.0) :fire: *De nombreux modes d’éclairage, de 1 à 1 750 lumens ! Cellule d’accumulateurs 18650 très facile à trouver d'occasion.*  
  ![nitecore hc-65 v2](/illustrations/nitecore hc-65 v2.jpg)  

### Montres
- Casio G-Shock GW-5610 [:notebook:](https://www.g-shock.eu/fr/montres/the-origin/gw-m5610u-1er/) [:money_with_wings:](https://ocarat.com/montre-g-shock-gw-m5610u-1er-casio-g-shock-77723.html) :fire: *Monstre solaire radiocontrolée. C’est le CHOC !*  
- Casio G-Shock GW-9400 [:notebook:](https://www.casio.com/fr/watches/gshock/product.GW-9400-1/) [:money_with_wings:](https://ocarat.com/montre-casio-g-shock-gw-9400-1er-36734.html) :fire: *Monstre solaire radiocontrolée… Boussoule, altimètre… LA montre-outils !*  

## Jeux

### Casses-têtes
- MeiLong 3×3 [:notebook:](https://www.thecubicle.com/collections/3x3-speed-cubes/products/mfjs-meilong-3x3) [:money_with_wings:](https://www.variantes.com/cubes-magiques-boutique/52633-cube-3x3-basic-stickerless-weilong-9999000007328.html) :fire: *Un produit très peu cher… qui a le [record du monde](https://www.worldcubeassociation.org/results/rankings/333/single) !*  

## Maison

### Soin de soi

- Moser 1400 [:notebook:](https://www.moserpro.fr/index.php?tondeuse-coupe-1400-fr) [:money_with_wings:](https://www.laboutiqueducoiffeur.com/tondeuse-de-coupe-1400-professionnal.html) :fire: *Produit professionnel. Pièces de rechange disponibles à la vente. Filaire.*

### Cuisine

- Ciseaux universels Fiskars [:notebook:](https://www.fiskars.com/fr-fr/loisirs-creatifs/produits/ciseaux/ciseaux-classic-universels-21cm-1005148) [:money_with_wings:](https://www.rascol.com/ciseaux-fiskars-classic-universels-21-cm-p-220999) :fire: *Des ciseaux avec un beau design, ergonomiques, durables et efficaces.*  
![Fiskars](/illustrations/fiskar.jpg) 
