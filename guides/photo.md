# GUIDE PHOTOGRAPHIE

Pour choisir un appareil photo, on peut choisir en function de ses caractéristiques.

## Caractéristiques photographiques

### L'ouverture maximale 

L'*ouverture maximale* indique à quel point l'objectif peut s'ouvrir pour laisser passer de la lumière.

Une ouverture plus grande (exemple : f/1.4) permet de réduire la profondeur de champ, pour rendre le fond flou lors d'un portrait par exemple.
Elle permettra aussi de meilleures photos dans la pénombre.

Important, il faut toujours regarder la valeur « équivalent 35 mm » pour pouvoir comparer l'ouverture entre deux appareils photos, car son effet est plus faible pour les plus petits capteurs (35 mm est une taille de capteur).

### La distance focale

La *distance focale* détermine l'angle de vue et peut être réglée avec le zoom.

Une petite distance (exemple : 17 mm) donne un grand angle et est pratique pour les paysages.
Une grande distance (exemple : 250 mm) permet de prendre des photos de loin, c'est utile pour photographier des animaux sauvages ou des évènements sportifs.

Attention, Les appareils avec un objectif à focale fixe comme le X100F ou le GR III n'ont pas de zoom !

### La taille physique du capteur

La *taille physique du capteur* (exemple : 1") détermine quelle quantité de lumière l'appareil pourra recevoir en un temps donné.

Plus le capteur est grand, moins les photos seront floues dans la pénombre.

Comparaison de différentes tailles :

![Différentes tailles de capteur](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Sensor_sizes_overlaid_inside.svg/1200px-Sensor_sizes_overlaid_inside.svg.png)

Source: [Sensor_sizes_overlaid.svg: Moxfyrederivative work: Autopilot](https://commons.wikimedia.org/wiki/File:Sensor_sizes_overlaid_inside.svg), [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0), via Wikimedia Commons

## Les caractéristiques du boîtier

Un **viseur** permet de coller l'appareil à son œil et ne voir que ce qui est dans la photo.

Un **boîtier de petite taille** aura plus de chances que tu l'emmène avec toi !

Les **contrôles manuels** de l'ouverture, la distance focale, l'exposition... sont importants si l'on souhaite prendre souvent des photos en contrôlant soi-même ces réglages.

## Bon à savoir

Le **nombre de mégapixels** n'est pas important car au delà de 8 mégapixels, les pixels restent invisible sauf en zoomant énormément ou an imprimant une photo sur un mur de 2 mètres.

La **couche logicielle** est différente entre chaque marque d'appareil.
Par exemple, les appareils Sony ont le meilleur autofocus mais sont peu intuitifs, et Fujifilm permet de donner un certain look au photos depuis l'appareil
